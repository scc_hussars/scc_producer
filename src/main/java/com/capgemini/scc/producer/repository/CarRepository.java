package com.capgemini.scc.producer.repository;

import com.capgemini.scc.producer.model.Car;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CarRepository {

    private List<Car> cars;

    public CarRepository() {
        cars = new ArrayList<>();
    }

    public List<Car> getCars() {
        return cars;
    }

    public Car addCar(Car car) {
        cars.add(car);
        return car;
    }
}
