package com.capgemini.scc.producer.controller;

import com.capgemini.scc.producer.exception.CarNotFoundException;
import com.capgemini.scc.producer.model.Car;
import com.capgemini.scc.producer.model.Color;
import com.capgemini.scc.producer.service.CarService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cars")
public class CarController {

    private CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Car> getCars(@RequestParam(name="colorName", required = false) Color color) throws CarNotFoundException {
        List<Car> cars = carService.getCars(Optional.ofNullable(color));
        if (cars.isEmpty()) {
            throw new CarNotFoundException("There are no cars with color: " + color);
        }

        return cars;
    }

    @PostMapping("")
    public Car addCar(Car car) {
        return carService.addCar(car);
    }
}
