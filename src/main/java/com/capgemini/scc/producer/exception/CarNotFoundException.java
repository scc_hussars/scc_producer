package com.capgemini.scc.producer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CarNotFoundException extends Exception {
    public CarNotFoundException(String message) {
        super(message);
    }
}
