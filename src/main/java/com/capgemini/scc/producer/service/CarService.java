package com.capgemini.scc.producer.service;

import com.capgemini.scc.producer.model.Car;
import com.capgemini.scc.producer.model.Color;
import com.capgemini.scc.producer.repository.CarRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarService {

    private CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> getCars(Optional<Color> color) {
        return carRepository.getCars()
                .stream()
                .filter(car -> !color.isPresent() || color.get().equals(car.getColor()))
                .collect(Collectors.toList());
    }

    public Car addCar(Car car) {
        return carRepository.addCar(car);
    }

}
