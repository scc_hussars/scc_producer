package com.capgemini.scc.producer.model;

public class Car {
    private String brand;
    private Color color;

    public Car(String brand, Color color) {
        this.brand = brand;
        this.color = color;
    }

    public Car() {
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
