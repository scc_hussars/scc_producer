package com.capgemini.scc.producer.model;

public enum Color {
    RED, GREEN, BLUE, BLACK, WHITE
}
