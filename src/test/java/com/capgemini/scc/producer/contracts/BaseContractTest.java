package com.capgemini.scc.producer.contracts;

import com.capgemini.scc.producer.SccProducerApplication;
import com.capgemini.scc.producer.controller.CarController;
import com.capgemini.scc.producer.model.Car;
import com.capgemini.scc.producer.model.Color;
import com.capgemini.scc.producer.service.CarService;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SccProducerApplication.class)
public abstract class BaseContractTest {

    private CarController carController;

    @Mock
    private CarService carService;

    @Before
    public void setup() {
        carController = new CarController(carService);
        setupMocks();
        RestAssuredMockMvc.standaloneSetup(carController);
    }

    private void setupMocks() {
        when(carService.getCars(any()))
                .thenAnswer(getMockCarList());
    }

    private Answer<Object> getMockCarList() {
        return invocation -> {
            Optional<Color> colorOptional = invocation.getArgument(0);
            if (colorOptional.isPresent()) {
                Color color = colorOptional.get();
                if (color != Color.WHITE)
                    return Arrays.asList(new Car("Audi", color));
            }

            return Lists.emptyList();
        };
    }
}
