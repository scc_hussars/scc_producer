package contracts.carrental.positive

org.springframework.cloud.contract.spec.Contract.make {
    priority 0
    request {
        method 'GET'
        urlPath("/cars") {
            queryParameters {
                parameter 'colorName': "WHITE"
            }
        }
    }
    response {
        status 400
    }
}