package contracts.carrental.positive

org.springframework.cloud.contract.spec.Contract.make {
    priority 9
    request {
        method 'GET'
        urlPath("/cars") {
            queryParameters {
                parameter 'colorName': value(regex("(BLACK|GREEN|RED|BLUE)"))
            }
        }
    }
    response {
        status OK()
        body("""
            [
              {
                "brand": "Audi",
                "color": "${fromRequest().query("colorName")}"
              }
            ]
        """
        )
        headers {
            contentType(applicationJsonUtf8())
        }
    }
}